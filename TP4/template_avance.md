# Template avancé

Ecrire une classe Mean qui permet de faire la moyenne de plusieurs
valeurs d'un type quelconque.

Le type devra fournir:
+= et la division par un entier

Exemple d'utilisation
```c++
	Mean<float> m2;
	m2.accum(70);
	m2.accum(50);
	m2.accum(97);
	std::cout << float(m2.get_mean()) << std::endl;
```

## V1 une classe template

Remarque: Que se passe-t-il avec des int8_t ?


## V2 une classe template + une spécialisation (int8_t)

Remarque: Que se passe-t-il avec des int16_t ?

Il faudrait écrire toutes les spécialisations !

Comment ne pas ré-écrire tout le code des spécialisation

##V3 
- une classe de base (données + type)
- n spécialisations
- une classe qui ajoute les méthodes 
- un using


##V4 type conditionnel

On peut réduire le code en limitant la spécialisation au type

- écrire une struct  template qui définit le type de son param.
- en faire n spécialisations (qui définissent int64_t et double)
- une classe Mean template


##V5 SFINAE

On peut encore réduire le code en utilisant enable_if

- écrire une struct  template qui définit le type de son param.
- en faire 2 (ou 3) spécialisations 
- une classe Mean template









