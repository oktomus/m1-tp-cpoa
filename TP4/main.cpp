#include "Mean.h"

#include <iostream>

int main(){

    Mean<float> m2;
    m2.accum(70);
    m2.accum(50);
    m2.accum(97);
    std::cout << m2.get_mean() << std::endl;

    Mean<int8_t> m3;
    m3.accum(0);
    m3.accum(127);
    m3.accum(128); // -127 !!
    m3.accum(-127); // -127 !!
    std::cout << int(m3.get_mean()) << std::endl;

    return 0;
}
