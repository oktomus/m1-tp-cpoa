#ifndef __MEAN__
#define __MEAN__

#include <cstdint>
#include <vector>

template <typename T, typename Enable=void>
struct SumType
{
    using type = T;
};


template <typename T>
struct SumType<T, typename std::enable_if<std::is_floating_point<T>::value, void>::type>
{
    using type = double;
};


/*
template <>
struct SumType<int8_t>
{
    using type = int64_t;
};

template <>
struct SumType<int16_t>
{
    using type = int64_t;
};
*/

// V4 On peut remettre les fonctions dans la classe principale

template <class T>
class MeanData{

public:
    using Data = T;
protected:

    typename SumType<T>::type _sum;
    int _nb;

};

/*
   V3
template <>
class MeanData<int8_t>{

public:
    using Data = int8_t;
protected:

    int64_t _sum;
    int _nb;

};

template <>
class MeanData<int16_t>{

public:
    using Data = int16_t;
protected:

    int64_t _sum;
    int _nb;

};
*/

template <typename T>
using M = MeanData<T>;

template <class M>
class MeanFunc : public M
{
public:

    using T = typename M::Data;

    MeanFunc(){
        this->_sum = 0;
        this->_nb = 0;
    }

    void accum(const T &val)
    {
        this->_sum += val;
        this->_nb++;
    }

    T get_mean() const
    {
        return this->_sum / this->_nb;
    }

};

template <typename T>
using Mean = MeanFunc<MeanData<T>>;

#endif
