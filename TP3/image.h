#ifndef __IMAGE__
#define __IMAGE__

/*

   ▀                               
 ▄▄▄    ▄▄▄▄▄   ▄▄▄    ▄▄▄▄   ▄▄▄  
   █    █ █ █  ▀   █  █▀ ▀█  █▀  █ 
   █    █ █ █  ▄▀▀▀█  █   █  █▀▀▀▀ 
 ▄▄█▄▄  █ █ █  ▀▄▄▀█  ▀█▄▀█  ▀█▄▄▀ 
                       ▄  █        
                        ▀▀         

*/

/**
  * P: The type for pixels
  */
template <class P>
class Image
{
public:

    Image(const size_t pwidth, const size_t pheight) : 
        width(width_), height(height_)
    {
        this->width_ = pwidth;
        this->height_ = pheight;
        pixels_ = new P[pwidth * pheight];
    }

    ~Image()
    {
        delete pixels_;
    }

    const size_t &width;
    const size_t &height;

    const P * get_pixels()
    {
        return pixels_;
    }

    P& pix(const size_t x, const size_t y)
    {
        return pixels_[y*width_ + x];
    }

    const P& pix(const size_t x, const size_t y) const
    {
        return pixels_[y*width_ + x];
    }

private:

    Image();
    Image(const Image &);

    size_t width_, height_;

    P * pixels_;

};

using ImageUC = Image<unsigned char>;

#endif
