#include <iostream>
#include <image.h>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

TEST(Image, testFirst)
{
    ASSERT_EQ(1, 1);
}

TEST(Image, testSize)
{
    ImageUC img(500, 400);

    ASSERT_EQ(img.width, 500);
    ASSERT_EQ(img.height, 400);
}

TEST(Image, testPixAffect)
{
    ImageUC img(10, 10);

    img.pix(3, 3) = 255;

    ASSERT_EQ(img.pix(3, 3), 255);

    const unsigned char * pixel = img.get_pixels() + 33 * sizeof(unsigned char);
    ASSERT_EQ((*pixel), 255);
}

int main(int argc, char **argv)
{
	testing::InitGoogleTest(&argc, argv);
	setlocale(LC_CTYPE, "");
	return RUN_ALL_TESTS();
}
