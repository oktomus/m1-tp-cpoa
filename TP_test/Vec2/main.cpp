#include <iostream>
#include <vec2f.h>

#include <gmock/gmock.h>
#include <gtest/gtest.h>

TEST(VecTest, test0Cpt)
{
    EXPECT_EQ(Vec2f::cpt(), 0);
    Vec2f v1(1 ,.5);
    Vec2f v2(6 ,3);
    EXPECT_EQ(Vec2f::cpt(), 2);

}

TEST(VecTest, testEq)
{
	Vec2f v1(1,1);
	Vec2f v2(1,1);
	EXPECT_TRUE(v1==v2);
	EXPECT_TRUE(v2==v1);
}

TEST(VecTest, testEq2)
{
	Vec2f v1(1,1);
	Vec2f v3(2,3);
	EXPECT_FALSE(v1==v3);
}

TEST(VecTest, testEq3)
{
    Vec2f v1;
    Vec2f v3(0,0);
    EXPECT_TRUE(v1==v3);
}

TEST(VecTest, testAssign)
{
    Vec2f v1(0, 0);
    EXPECT_FLOAT_EQ(v1.x, 0);
    EXPECT_FLOAT_EQ(v1.y, 0);
    Vec2f v3(1,1);
    v1 = v3;
    EXPECT_FLOAT_EQ(v1.x, 1);
    EXPECT_FLOAT_EQ(v1.y, 1);

    Vec2f v4(-0.5,98.65);
    v1 = v4;
    EXPECT_FLOAT_EQ(v1.x, -0.5);
    EXPECT_FLOAT_EQ(v1.y, 98.65);
}


TEST(VecTest, testPlusEqualVec)
{
    Vec2f v1(0 ,0);

    EXPECT_FLOAT_EQ(v1.x, 0);
    EXPECT_FLOAT_EQ(v1.y, 0);

    Vec2f v3(1,1);
    v1 += v3;

    EXPECT_FLOAT_EQ(v1.x, 1);
    EXPECT_FLOAT_EQ(v1.y, 1);
    EXPECT_FLOAT_EQ(v3.x, 1);
    EXPECT_FLOAT_EQ(v3.y, 1);

    Vec2f v4(0.72,-2.6);
    v1 += v4;

    EXPECT_FLOAT_EQ(v1.x, 1.72);
    EXPECT_FLOAT_EQ(v1.y, -1.6);
    EXPECT_FLOAT_EQ(v4.x, 0.72);
    EXPECT_FLOAT_EQ(v4.y, -2.6);

}

TEST(VecTest, testPlusVec)
{
    Vec2f v1(0 ,0);

    EXPECT_FLOAT_EQ(v1.x, 0);
    EXPECT_FLOAT_EQ(v1.y, 0);

    Vec2f v3(1,1);
    Vec2f v4(100.001,-5);

    v1 = v3 + v4;

    EXPECT_FLOAT_EQ(v1.x, 101.001);
    EXPECT_FLOAT_EQ(v1.y, -4);

    EXPECT_FLOAT_EQ(v3.x, 1);
    EXPECT_FLOAT_EQ(v3.y, 1);
    EXPECT_FLOAT_EQ(v4.x, 100.001);
    EXPECT_FLOAT_EQ(v4.y, -5);

    Vec2f v(v3 + v4);


    EXPECT_FLOAT_EQ(v.x, 101.001);
    EXPECT_FLOAT_EQ(v.y, -4);

    EXPECT_FLOAT_EQ(v3.x, 1);
    EXPECT_FLOAT_EQ(v3.y, 1);
    EXPECT_FLOAT_EQ(v4.x, 100.001);
    EXPECT_FLOAT_EQ(v4.y, -5);

}

TEST(VecTest, testMultScalaire)
{
    Vec2f v1(0 ,0);
    v1 *= 1;

    EXPECT_FLOAT_EQ(v1.x, 0);
    EXPECT_FLOAT_EQ(v1.y, 0);

    Vec2f v4(2, .5);
    v1 = v4 * 2;

    EXPECT_FLOAT_EQ(v1.x, 4);
    EXPECT_FLOAT_EQ(v1.y, 1);
    EXPECT_FLOAT_EQ(v4.x, 2);
    EXPECT_FLOAT_EQ(v4.y, .5);

    Vec2f v = v1 * 10;

    EXPECT_FLOAT_EQ(v.x, 40);
    EXPECT_FLOAT_EQ(v.y, 10);
    EXPECT_FLOAT_EQ(v1.x, 4);
    EXPECT_FLOAT_EQ(v1.y, 1);

}

TEST(VecTest, testMultScalaireRevert)
{
    Vec2f v1(0 ,0);
    v1 *= 1;

    EXPECT_FLOAT_EQ(v1.x, 0);
    EXPECT_FLOAT_EQ(v1.y, 0);

    Vec2f v4(2, .5);
    v1 = 2 * v4;

    EXPECT_FLOAT_EQ(v1.x, 4);
    EXPECT_FLOAT_EQ(v1.y, 1);
    EXPECT_FLOAT_EQ(v4.x, 2);
    EXPECT_FLOAT_EQ(v4.y, .5);

    Vec2f v = 10 * v1;

    EXPECT_FLOAT_EQ(v.x, 40);
    EXPECT_FLOAT_EQ(v.y, 10);
    EXPECT_FLOAT_EQ(v1.x, 4);
    EXPECT_FLOAT_EQ(v1.y, 1);

}

TEST(VecTest, testArray)
{
    Vec2f v1(1 ,0);


    EXPECT_FLOAT_EQ(v1[0], 1);
    EXPECT_FLOAT_EQ(v1[1], 0);

    v1[0] = 500.12;
    v1[1] = -3;

    EXPECT_FLOAT_EQ(v1[0], 500.12);
    EXPECT_FLOAT_EQ(v1[1], -3);
    EXPECT_FLOAT_EQ(v1.x, 500.12);
    EXPECT_FLOAT_EQ(v1.y, -3);
}

TEST(VecTest, testDotProduct)
{
    Vec2f v1(1 ,0);
    Vec2f v2(6 ,3);

    EXPECT_FLOAT_EQ(v1.dot(v2), 6);
}

TEST(VecTest, testCrossProduct)
{
    Vec2f v1(1 ,.5);
    Vec2f v2(6 ,3);

    EXPECT_FLOAT_EQ(v1.cross(v2), 0);
}






// test sur les entiers
//	EXPECT_EQ, EXPECT_NE, EXPECT_GT, EXPECT_GE, EXPECT_LT, EXPECT_LE, EXPECT_GT

// test sur les réels
//	EXPECT_FLOAT_EQ, EXPECT_DOUBLE_EQ, EXPECT_NEAR

// si on remplace EXPECT par ASSERT le test s'arrete si la condition n'est pas respectee




int main(int argc, char **argv)
{
	testing::InitGoogleTest(&argc, argv);
	setlocale(LC_CTYPE, "");
	return RUN_ALL_TESTS();
}
