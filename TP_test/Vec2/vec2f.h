#ifndef VEC2F_H
#define VEC2F_H

#include <iostream>


class Vec2f
{

private:

    float _x, _y;
    static size_t _cpt;

public:
	Vec2f();
    ~Vec2f() = default;

	Vec2f(float x, float y);
    Vec2f(const Vec2f& b);

    bool operator== (const Vec2f& v);

    Vec2f& operator+= (const Vec2f& b);
    friend Vec2f operator +(Vec2f a, const Vec2f& b);

    Vec2f& operator*= (const float& b);
    friend Vec2f operator* (Vec2f a, const float& b);
    friend Vec2f operator* (const float& b, Vec2f a);


    Vec2f& operator= (const Vec2f&);

    const float operator[] (const size_t) const;
    float& operator[] (const size_t);


    friend std::ostream& operator<< (std::ostream& a, const Vec2f& b);

    float dot(const Vec2f&) const;
    float cross(const Vec2f&) const;

    // Getters
    const float &x;
    const float &y;

    static size_t cpt()
    {
        return _cpt;
    }





};

#endif // VEC2F_H
