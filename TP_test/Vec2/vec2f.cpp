#include "vec2f.h"

size_t Vec2f::_cpt = 0;

Vec2f::Vec2f() : x(_x), y(_y)
{
    _x = 0;
    _y = 0;
    _cpt++;
}

Vec2f::Vec2f(float x,  float y) : x(_x), y(_y)
{
    _x = x;
    _y = y;
    _cpt++;
}

Vec2f::Vec2f(const Vec2f& b) : x(_x), y(_y)
{
    _x = b.x;
    _y = b.y;
    _cpt++;
}



bool Vec2f::operator== (const Vec2f& v)
{
    return x == v.x && y == v.y;
}

// -------------------------   +

Vec2f& Vec2f::operator+= (const Vec2f& b)
{
    _x += b.x;
    _y += b.y;
    return *this;
}

Vec2f operator +(Vec2f a, const Vec2f& b)
{
    a += b;
    return a;
}

// -------------------------   *

Vec2f& Vec2f::operator*= (const float& b)
{
    _x *= b;
    _y *= b;
    return *this;
}


Vec2f operator* (Vec2f a, const float& b)
{
    a._x *= b;
    a._y *= b;
    return a;
}

Vec2f operator* (const float& b, Vec2f a)
{
    a._x *= b;
    a._y *= b;
    return a;
}

// -------------------------   =

Vec2f& Vec2f::operator=(const Vec2f& b)
{
    _x = b.x;
    _y = b.y;
    return *this;
}

// -------------------------   []

const float Vec2f::operator[](const size_t a) const
{
    if (a == 1) return y;
    return x;
}

float &Vec2f::operator[](const size_t a)
{
    if (a == 1) return _y;
    return _x;
}

float Vec2f::dot(const Vec2f &b) const
{
    return x * b.x + y * b.y;
}

float Vec2f::cross(const Vec2f &b) const
{
     return x * b.y - y * b.x;
}

std::ostream& operator<< (std::ostream& a, const Vec2f& b)
{
    return a << "(" << b.x << ", " << b.y << ")";
}

