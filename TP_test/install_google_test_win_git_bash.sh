#!/bin/bash

# recupere le code de google-test
#git clone https://github.com/google/googletest

# nettoie le repertoire de build
rm -rf build_gt
mkdir build_gt/
cd build_gt

# compile et install (dans le rep. GT) 
cmake -G "Visual Studio 15 2017 Win64" ../googletest/ -DBUILD_GTEST=true -DCMAKE_INSTALL_PREFIX=../GT
cmake --build . --config Debug --target install
cmake --build . --config Release --target install


cd ../
rm -rf build_Vec2
mkdir build_Vec2/
cd build_Vec2

cmake -G "Visual Studio 15 2017 Win64" ../Vec2/ 