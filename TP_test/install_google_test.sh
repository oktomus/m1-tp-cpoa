#!/bin/bash

# recupere le code de google-test
#git clone https://github.com/google/googletest

# nettoie le repertoire de build
rm -rf build_gt
mkdir build_gt/
cd build_gt

export MAKEFLAGS=-j`nproc` 
# compile et install (dans le rep. GT) 
# en Debug
cmake ../googletest/ -DCMAKE_BUILD_TYPE=Debug -DBUILD_GTEST=true -DCMAKE_INSTALL_PREFIX=../GT
cmake --build . --target install
# en Releaqz
cmake . -DCMAKE_BUILD_TYPE=Release -DBUILD_GTEST=true -DCMAKE_INSTALL_PREFIX=../GT
cmake --build . --target install