#ifndef __CSG_NODE__
#define __CSG_NODE__

#include <string>

typedef int matrix33d;

class CsgNode
{

protected:
    std::string label;
    size_t id;
    CsgNode * parent;
    static size_t maxid;

public:

    CsgNode()
    {
        parent = 0;
        id = CsgNode::maxid++;
        label = "Noname";
        printf("Noeud %s[id: %d, label: %s] construit.\n",
                this->type(),
                this->id,
                this->label.c_str()
              );
    }

    ~CsgNode()
    {
        printf("Destruction de Noeud %s[id: %d, label: %s].\n",
                this->type(),
                this->id,
                this->label.c_str()
              );
    }

    CsgNode(const CsgNode& other) : CsgNode()
    {
        parent = other.parent;
        id = CsgNode::maxid++;
        label = other.label;
        printf("Noeud %s[id: %d, label: %s] construit.\n",
                this->type(),
                this->id,
                this->label.c_str()
              );
    }

    const std::string & getLabel() const
    {
        return label;
    }

    const size_t & getId() const
    {
        return id;
    }

    const CsgNode*  getParent() const
    {
        return parent;
    }

    void setLabel(std::string plabel)
    {
        label = plabel;
    }

    void setId(size_t pid)
    {
        id = pid;
    }

    void setParent(CsgNode * pparent)
    {
        parent = pparent;
    }

    virtual const char * type() const
    {
        return "CsgNode";
    }

};

#endif
