#ifndef __CSG_OPERATION__
#define __CSG_OPERATION__

#include "csgNode.h"

enum OPERATION
{
    UNION, INTERSECTION, DIFFERENCE
};

class CsgOperation : public CsgNode
{

    OPERATION operation;

public:

    CsgOperation() : CsgNode()
    {
        operation = OPERATION::UNION;
        printf("Noeud %s[id: %d, label: %s] construit.\n",
                this->type(),
                this->id,
                this->label.c_str()
              );
    }

    CsgOperation(const CsgOperation & other) : CsgNode(other)
    {
        operation = other.operation;
        printf("Noeud %s[id: %d, label: %s] construit.\n",
                this->type(),
                this->id,
                this->label.c_str()
              );
    }

    ~CsgOperation() 
    {
        printf("Destruction de Noeud %s[id: %d, label: %s].\n",
                this->type(),
                this->id,
                this->label.c_str()
              );
    }

    const char * type() const
    {
        return "CsgOperation";
    }

};


#endif
