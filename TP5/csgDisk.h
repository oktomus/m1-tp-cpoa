#ifndef __CSG_DISK__
#define __CSG_DISK__

#include "csgPrimitive.h"


class CsgDisk : public CsgPrimitive
{

public:

    CsgDisk() : CsgPrimitive()
    {
        printf("Noeud %s[id: %d, label: %s] construit.\n",
                this->type(),
                this->id,
                this->label.c_str()
              );
    }

    CsgDisk(const CsgDisk & other) : CsgPrimitive(other)
    {
        printf("Noeud %s[id: %d, label: %s] construit.\n",
                this->type(),
                this->id,
                this->label.c_str()
              );
    }

    ~CsgDisk() 
    {
        printf("Destruction de Noeud %s[id: %d, label: %s].\n",
                this->type(),
                this->id,
                this->label.c_str()
              );
    }

    const char * type() const
    {
        return "CsgDisk";
    }

};


#endif
