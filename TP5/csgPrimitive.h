#ifndef __CSG_PRIMITIVE__
#define __CSG_PRIMITIVE__

#include "csgNode.h"


class CsgPrimitive : public CsgNode
{
    matrix33d transform;

public:

    CsgPrimitive() : CsgNode()
    {
        printf("Noeud %s[id: %d, label: %s] construit.\n",
                this->type(),
                this->id,
                this->label.c_str()
              );
    }

    CsgPrimitive(const CsgPrimitive & other) : CsgNode(other)
    {
        transform = other.transform;
        printf("Noeud %s[id: %d, label: %s] construit.\n",
                this->type(),
                this->id,
                this->label.c_str()
              );
    }

    ~CsgPrimitive() 
    {
        printf("Destruction de Noeud %s[id: %d, label: %s].\n",
                this->type(),
                this->id,
                this->label.c_str()
              );
    }

    virtual const char * type() const
    {
        return "CsgPrimivite";
    }

};


#endif
