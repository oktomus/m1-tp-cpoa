#include <iostream>
#include <vector>
#include "csgNode.h"
#include "csgPrimitive.h"
#include "csgRegularPolygon.h"
#include "csgDisk.h"
#include "csgOperation.h"
#include <experimental/random>

/*
int main()
{

    {
        std::cout << "A)\n";
        CsgNode * node = new CsgNode();
        std::cout << std::endl;
        CsgPrimitive * prim = new CsgPrimitive();
        std::cout << std::endl;
        CsgDisk * disk = new CsgDisk();
        std::cout << std::endl;
        CsgRegularPolygon * regular = new CsgRegularPolygon();
        std::cout << std::endl;
        CsgOperation * op = new CsgOperation();

        delete node;
        std::cout << std::endl;
        delete prim;
        std::cout << std::endl;
        delete disk;
        std::cout << std::endl;
        delete regular;
        std::cout << std::endl;
        delete op;
        std::cout << std::endl;

    }

    {
        std::cout << "B)\n";
        CsgNode * node = new CsgNode();
        CsgDisk * disk = new CsgDisk();
        CsgOperation * op = new CsgOperation();

        {
            std::cout << " Node to Primitive \n";
            CsgPrimitive * primNode = static_cast<CsgPrimitive*>(node);
            // Downcast
            std::cout << " Primitive to Node \n";
            CsgNode * nodePrim = dynamic_cast<CsgNode*>(primNode);
            if(!nodePrim) std::cout << "Cast failed! \n";

            delete primNode;
            delete nodePrim;
        }

        {
            std::cout << " Node to Disk \n";
            CsgDisk * a = static_cast<CsgDisk*>(node);
            // Downcast
            std::cout << " Disk to Node \n";
            CsgNode * b = dynamic_cast<CsgNode*>(a);
            if(!b) std::cout << "Cast failed! \n";

            delete a;
            delete b;
        }
        {
            std::cout << " Disk to Operation \n";
            CsgOperation * a = dynamic_cast<CsgOperation*>(disk);
            if(!a) std::cout << "Cast failed! \n";
            // Downcast
            std::cout << " Operation to Disk \n";
            CsgDisk * b = dynamic_cast<CsgDisk*>(op);
            if(!b) std::cout << "Cast failed! \n";

            delete a;
            delete b;
        }
        delete node;
        delete disk;
        delete op;
    }

    {
        std::cout << "C)\n";
        CsgNode * node = new CsgNode();
        std::cout << "Node is of type " << node->type() << "\n";
        std::cout << std::endl;
        CsgNode * prim = new CsgPrimitive();
        std::cout << "Prim is of type " << prim->type() << "\n";
        std::cout << std::endl;
        CsgNode * disk = new CsgDisk();
        std::cout << "Disk is of type " << disk->type() << "\n";
        std::cout << std::endl;
        CsgNode * regular = new CsgRegularPolygon();
        std::cout << "Poly is of type " << regular->type() << "\n";
        std::cout << std::endl;
        CsgNode * op = new CsgOperation();
        std::cout << "Op is of type " << op->type() << "\n";
        std::cout << std::endl;

        delete node;
        std::cout << std::endl;
        delete prim;
        std::cout << std::endl;
        delete disk;
        std::cout << std::endl;
        delete regular;
        std::cout << std::endl;
        delete op;
        std::cout << std::endl;

    }

    return 0;
}
*/

CsgNode * randomPrimitive()
{
    int r = std::experimental::randint(0, 1);
    if(r)
        return new CsgRegularPolygon();
    else
        return new CsgDisk();

}

int main()
{

    printf("Programme 2\n");

    std::vector<CsgNode *> tabNodes;
    int choice;

    std::cout << "0:Primitive aleatoire, 1:Union, 2: Intersection, 3: Difference\n";
    while(std::cin >> choice)
    {
        if(choice == 0)
        {
            std::cout << "Creation primitive";
            tabNodes.push_back(randomPrimitive());
            continue;
        }
        int nodeA, nodeB;
        std::cout << "Inidice du permier node: ";
        std::cin >> nodeA;
        std::cout << "Inidice du second node: ";
        std::cin >> nodeB;
        CsgNode * operation = new CsgOperation();
        tabNodes.push_back(operation);
    }

    return 0;
}
