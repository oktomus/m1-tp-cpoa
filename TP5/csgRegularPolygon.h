#ifndef __CSG_REGULAR_POLYGON__
#define __CSG_REGULAR_POLYGON__

#include "csgPrimitive.h"

class CsgRegularPolygon : public CsgPrimitive
{

    int nbfaces;

public:
    CsgRegularPolygon() : CsgPrimitive()
    {
        nbfaces = 0;
        printf("Noeud %s[id: %d, label: %s] construit.\n",
                this->type(),
                this->id,
                this->label.c_str()
              );
    }

    CsgRegularPolygon(const CsgRegularPolygon & other) : CsgPrimitive(other)
    {
        nbfaces = other.nbfaces;;
        printf("Noeud %s[id: %d, label: %s] construit.\n",
                this->type(),
                this->id,
                this->label.c_str()
              );
    }

    ~CsgRegularPolygon() 
    {
        printf("Destruction de Noeud %s[id: %d, label: %s].\n",
                this->type(),
                this->id,
                this->label.c_str()
              );
    }

    const char * type() const
    {
        return "CsgRegularPolygon";
    }


};

#endif
