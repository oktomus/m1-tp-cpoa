cmake_minimum_required( VERSION 3.0 )

project( Stack )

# Retrieve source files
file( GLOB incList "${CMAKE_CURRENT_SOURCE_DIR}/*.h" )
file( GLOB srcList "${CMAKE_CURRENT_SOURCE_DIR}/*.cpp" )

# Target program
add_executable( ${PROJECT_NAME} ${srcList} ${incList} )
