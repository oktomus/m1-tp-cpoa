#include <iostream>
#include <random>
#include <string>
#include <vector>
#include <algorithm>

#include "personnage.h"
#include "nosenseforce.h"
#include "rebelle.h"
#include "imperial.h"
#include "jedi.h"
#include "sith.h"
#include "skywalker.h"

class SortPerso
{

    public:
        bool operator()(Personnage * left, Personnage * right)
        {
            SenseForce * sfleft = dynamic_cast<SenseForce*>(left);
            SenseForce * sfright = dynamic_cast<SenseForce*>(right);
            return sfleft && !sfright;
        }
};

class JediToSith
{
    public:
        Personnage * operator()(Personnage * p)
        {
            Jedi * jedip = dynamic_cast<Jedi*>(p);

            if(!jedip) return p;
            return new Sith(jedip->name(), jedip->attack());
        }

};

std::vector<Personnage*> chuter(std::vector<Personnage*> persos)
{
    std::vector<Personnage*> out;
    std::transform(
            persos.begin(), 
            persos.end(), 
            std::back_inserter(out), 
            JediToSith());
    return out;
}

int main()
{
    printf("TP_CPOA_2 Masson Kevin\n");

    std::vector<Personnage*> persos;

    // Creation
    persos.push_back(new Jedi("Mr Jedi 1", 0));
    persos.push_back(new Jedi("Mme Jedi 2", 1));
    persos.push_back(new Sith("Mme Sith 1", 1));
    persos.push_back(new Sith("Mme Sith 2", 0));
    persos.push_back(new Imperial("Mr Imperial 1", "contrebandier"));
    persos.push_back(new Imperial("Mr Imperial 2", "trooper"));
    persos.push_back(new Rebelle("Mr Rebelle 1", "punk"));
    persos.push_back(new Rebelle("Mme Rebelle 2", "barman"));

    // Suffle
    std::random_device rd;
    std::mt19937 seed(rd());
    std::shuffle(persos.begin(), persos.end(), seed);

    std::cout << "== Affichage des attaques\n";

    // Attaque
    auto it = persos.begin();
    while(it != persos.end())
    {
        (*it)->attaque();
        it++;
    }

    std::cout << "== Affichage de la sensibilité à la force\n";

    // Affichage si sensible à la force ou non
    it = persos.begin();
    while(it != persos.end())
    {
        std::cout << (*it)->name();
        SenseForce * sf = dynamic_cast<SenseForce*>((*it));
        if(!sf) 
            std::cout << " n'est pas sensible à la force.\n";
        else
            std::cout << " est sensible à la force.\n";
        it++;
    }

    std::cout << "== Triage suivant la sensibilité à la force\n";

    // Triage par force
    std::sort(persos.begin(), persos.end(), SortPerso());

    // Re affichage
    it = persos.begin();
    while(it != persos.end())
    {
        std::cout << (*it)->name();
        SenseForce * sf = dynamic_cast<SenseForce*>((*it));
        if(!sf) 
            std::cout << " n'est pas sensible à la force.\n";
        else
            std::cout << " est sensible à la force.\n";
        it++;
    }

    std::cout << "== Chute\n";
    // Jedi to Sith
    std::vector<Personnage*> persos2 = chuter(persos);

    it = persos2.begin();
    while(it != persos2.end())
    {
        (*it)->attaque();
        it++;
    }

    // Skywalker
    std::cout << "== Skywlaker\n";
    Skywalker * sky = new Skywalker();
    sky->attaque();
    NoSenseForce * skyNoForce = reinterpret_cast<NoSenseForce*>(sky);
    skyNoForce->attaque();
    SenseForce * skyForce = reinterpret_cast<SenseForce*>(skyNoForce);
    skyForce->attaque();


    // Free
    std::cout << "== Cleaning\n";

    for(int i = 0; i < persos.size(); i++)
    {
        delete persos[i];
    }

    for(int i = 0; i < persos2.size(); i++)
    {
        delete persos2[i];
    }
    delete sky;

    std::cout << "== Fin\n";

    return 0;
}
