#ifndef __NOSENSEFORCE__
#define __NOSENSEFORCE__

#include "personnage.h"
#include <string>

class NoSenseForce : public Personnage
{
public:

    NoSenseForce(const std::string& pNom, const std::string& pTitre) :
        Personnage(pNom)
    {
        this->titre_ = pTitre;
    }


protected:

    std::string blaster() const
    {
        return "tire au blaster";
    }
};

#endif
