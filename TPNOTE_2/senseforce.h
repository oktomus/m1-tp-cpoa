#ifndef __SENSEFORCE__
#define __SENSEFORCE__

#include "personnage.h"
#include <string>

class SenseForce : public Personnage
{
public:

    SenseForce(const std::string& pNom, const int& pAtck) :
        Personnage(pNom),
        prefferedAttack_(pAtck)
    {
    }

    int attack() const {
        return prefferedAttack_;
    }

protected:

    int prefferedAttack_ = 0;

    std::string sabreLaser() const
    {
        return "se bat au sabre laser";
    }

    std::string force() const
    {
        return "utilise la force";
    }
};

#endif
