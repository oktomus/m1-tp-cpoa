#ifndef __REBELLE__
#define __REBELLE__

#include <iostream>

#include "nosenseforce.h"

class Rebelle : public NoSenseForce
{

public:

    using NoSenseForce::NoSenseForce;

    void attaque() const
    {
        printf("%s, %s, %s.\n", nom_.c_str(), titre_.c_str(), blaster().c_str()); 
    }

};

#endif
