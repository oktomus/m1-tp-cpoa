#ifndef __PERSONNAGE__
#define __PERSONNAGE__

#include <string>

class Personnage
{

public:

    Personnage(const std::string& pNom) :
        nom_(pNom)
    {
    }

    virtual void attaque() const = 0;
    
    std::string name() const
    {
        return nom_;
    }

protected:

    std::string nom_;

    std::string titre_;




};

#endif
