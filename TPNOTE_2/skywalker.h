#ifndef __SKYWALKER__
#define __SKYWALKER____

#include "senseforce.h"
#include "nosenseforce.h"
#include <string>

class Skywalker : public SenseForce, public NoSenseForce
{
public:

    Skywalker() : 
        SenseForce("Skywalker", 0),
        NoSenseForce("Skywalker", "le Surhomme")
    {
        this->SenseForce::titre_ = "le Surhomme";
    }

    void attaque() const
    {
        std::cout << "Je suis " <<
            this->SenseForce::nom_ << 
            " " << 
            this->NoSenseForce::titre_ << 
            " et j'attaque comme je veux !\n";
    }

};

#endif

