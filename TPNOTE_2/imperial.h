#ifndef __IMPERIAL__
#define __IMPERIAL__

#include "nosenseforce.h"

class Imperial : public NoSenseForce
{

public:

    using NoSenseForce::NoSenseForce;

    void attaque() const
    {
        printf("%s, %s, %s.\n", nom_.c_str(), titre_.c_str(), blaster().c_str());
    }

};

#endif
