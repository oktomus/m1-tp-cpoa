#ifndef __JEDI__
#define __JEDI__

#include "senseforce.h"
#include <string>

class Jedi : public SenseForce
{
public:

    Jedi(const std::string& pNom, const int& pAtck) : 
        SenseForce(pNom, pAtck)
    {
        this->titre_ = "le jedi";
    }

    void attaque() const
    {
        std::string atk;
        switch(prefferedAttack_)
        {
            case (0):
                atk = sabreLaser();
                break;
            case (1):
                atk = force();
                break;
            default:
                atk = "undefined";
                break;
        };

        printf("%s, %s, %s.\n", nom_.c_str(), titre_.c_str(), atk.c_str());
    }

};

#endif
